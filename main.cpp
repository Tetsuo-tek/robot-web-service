#include "robotwebservice.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    skApp->init(2000, 250000, SK_TIMEDLOOP_RT);

    SkCli *cli = skApp->appCli();

    cli->add("--flow-url",                  "", "",         "Setup the FlowHUB url (tcp: or local:)");
    cli->add("--user",                      "", "guest",    "Setup the login username");
    cli->add("--password",                  "", "password", "Setup the login password");

    cli->add("--http-www",                  "", "",         "Enable the static HTTP service");
    cli->add("--http-www-path",             "", "www",      "Setup the static www directory");
    cli->add("--http-addr",                 "", "0.0.0.0",  "Setup the listen address for HTTP service");
    cli->add("--http-port",                 "", "20000",    "Setup the listen port for HTTP service");
    cli->add("--http-idle-timeout",         "", "20",       "Setup the HTTP idle-timeout interval in seconds");
    cli->add("--http-queued-connection",    "", "50",       "Setup the HTTP max count for queued sockets");
    cli->add("--http-max-header",           "", "1000000",  "Setup the max allowed header size");
    cli->add("--http-ssl",                  "", "",         "Enable HTTP on SSL");
    cli->add("--http-ssl-cert",             "", "",         "Setup the SSL certificate file");
    cli->add("--http-ssl-key",              "", "",         "Setup the SSL key file");

    new RobotWebService;
    return skApp->exec();
}
