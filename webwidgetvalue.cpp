#include "webwidgetvalue.h"
#include "webwidgetcontainer.h"

#include "Core/Scripting/SkCodeTemplate/sktemplatemanager.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WebWidgetValue, AbstractWebWidget)
{
    t = WebWidgetType::WT_WIDGET;
    source_t = WebWidgetSourceType::WST_NOTYPE;
    //chanID = -1;
    min = 0;
    max = 0;
}

void WebWidgetValue::setDataSourceType(WebWidgetSourceType type)
{
    source_t = type;
}

void WebWidgetValue::setID(CStr *widgetID)
{
    if (source_t == WST_VALUEWIDGET)
    {
        FlatError("CANNOT setup WID directly on a ValueWidget");
        return;
    }

    wid = widgetID;

    SkString n(wid);
    n.append("_W");
    setObjectName(n.c_str());
}

void WebWidgetValue::setPair(CStr *pairDbName, CStr *key)
{
    if (source_t != WST_VALUEWIDGET)
    {
        FlatError("Widget is NOT ValueWidget, CANNOT setup PairDbKey");
        return;
    }

    dbName = pairDbName;
    k = key;

    wid = dbName;
    wid.append("_");
    wid.append(k);

    SkString n(wid);
    n.append("_W");
    setObjectName(n.c_str());
}

void WebWidgetValue::setMin(const SkVariant &minimum)
{
    min = minimum;
}

void WebWidgetValue::setMax(const SkVariant &maximum)
{
    max = maximum;
}

void WebWidgetValue::setUdm(CStr *udm)
{
    u = udm;
}

void WebWidgetValue::addTarget(WebWidgetValue *w)
{
    w->setObjectName(w, "Target");
    w->setMin(min);
    w->setMax(max);
    w->setUdm(u.c_str());
    targetWidgets << w;
}

WebWidgetSourceType WebWidgetValue::widgetSourceType()
{
    return source_t;
}

CStr *WebWidgetValue::pairsDatabaseName()
{
    return dbName.c_str();
}

CStr *WebWidgetValue::pairKey()
{
    return k.c_str();
}

bool WebWidgetValue::isLimited()
{
    return max > min;
}

SkVariant &WebWidgetValue::getMin()
{
    return min;
}

SkVariant &WebWidgetValue::getMax()
{
    return max;
}

CStr *WebWidgetValue::udm()
{
    return u.c_str();
}

bool WebWidgetValue::hasTargets()
{
    return !targetWidgets.isEmpty();
}

SkList<WebWidgetValue *> &WebWidgetValue::targets()
{
    return targetWidgets;
}

void WebWidgetValue::setRunJsTemplate(CStr *jsTemplatePath, SkArgsMap &customize)
{
    runJs.filePath = jsTemplatePath;
    runJs.customize = customize;
}

bool WebWidgetValue::hasRunJsTemplate()
{
    return !runJs.filePath.isEmpty();
}

WebWidgetTemplate &WebWidgetValue::runJsTemplate()
{
    return runJs;
}

bool WebWidgetValue::buildRunJs(SkTemplateManager *manager, SkString &outputCode, SkString &val)
{
    SkJsTemplate *item = nullptr;
    WebWidgetContainer *wc = container();

    if (hasRunJsTemplate())
    {
        item = manager->newJs(runJs.filePath.c_str());
        item->setVariable("ID", wid.c_str());
        item->setVariable("LABEL", lbl.c_str());

        SkString s(wc->childrenWidgets().indexOf(this));
        item->setVariable("CHILD_INDEX", s.c_str());
        item->setVariable("PARENT_ID", wc->id());

        item->setVariable("VALUE", val.c_str());
        item->setVariable("UDM", u.c_str());

        if (!runJs.customize.isEmpty())
            item->setCustomization(runJs.customize);

        if (!item->build(outputCode))
            return false;
    }

    if (!targetWidgets.isEmpty())
    {
        SkAbstractListIterator<WebWidgetValue *> *itr = targetWidgets.iterator();

        while(itr->next())
        {
            WebWidgetValue *target = itr->item();

            //CODE WILL BE BUILT ONLY IF THIS target HAS runJs
            //RETURN false ONLY IF THIS target HAS NOT runJs OR THERE ARE ERRORs
            if (target->hasRunJsTemplate() && !target->buildRunJs(manager, outputCode, val))
            {
                ObjectError("Cannot build RunJs template for target: " << target->runJs.filePath);
                delete itr;
                return false;
            }
        }

        delete itr;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
