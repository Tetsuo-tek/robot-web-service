#include "robotwebservice.h"
#include "webfrontendmountpoint.h"
#include "agentmountpoint.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/TCP/HTTP/Server/skhttpservice.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skwsredistrmountpoint.h>
#include <Core/System/Network/TCP/HTTP/skwebsocket.h>
#include "Core/System/skdeviceredistr.h"
#include <Core/Containers/skarraycast.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(RobotWebService, SkFlowAsync)
{
    service = nullptr;
    wwwMp = nullptr;
    homeAppMp = nullptr;
    pairsMp = nullptr;
    frontendMp = nullptr;

    SlotSet(init);
    SlotSet(quit);

    SlotSet(onPairsPageAccepted);
    SlotSet(onStreamTargetAdded);
    SlotSet(onStreamTargetRemoved);

    SlotSet(fastTick);

    SlotSet(onAppExitRequest);

    setObjectName("RobotWebService");

    Attach(skApp->started_SIG, pulse, this, init, SkOneShotDirect);
    Attach(skApp->kernel_SIG, pulse, this, onAppExitRequest, SkOneShotQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotWebService, init)
{
    SilentSlotArgsWarning();

    AssertKiller(!skApp->appCli()->check());
    AssertKiller(!tryToConnect());
    AssertKiller(!startService());
}

SlotImpl(RobotWebService, quit)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting ..");

    if (isConnected())
        this->close();

    skApp->quit();
}

SlotImpl(RobotWebService, onAppExitRequest)
{
    SilentSlotArgsWarning();

    int32_t sig = skApp->kernel_SIG->pulse_SIGNAL.getParameters()[0].toInt32();

    if (sig != SIGINT
        && sig != SIGINT
        && sig != SIGTERM
        && sig != SIGQUIT)
    {
        return;
    }

    ObjectWarning("Forcing application shutdown ..");

    frontendMp->close();
    frontendMp = nullptr;

    homeAppMp = nullptr;
    wwwMp = nullptr;
    pairsMp = nullptr;
    frontendMp = nullptr;

    service->stop();
    service->destroyLater();
    service = nullptr;

    SkBinaryTreeVisit<SkFlowChanID, FlowRedistrMP *> *itr = redistrs.iterator();

    while(itr->next())
    {
        FlowRedistrMP *r = itr->item().value();

        if (isSubscribed(r->chanID))
            unsubscribeChannel(r->chanID);

        delete r;
    }

    delete itr;
    redistrs.clear();

    skApp->invokeSlot(quit_SLOT, this, this);

    ObjectMessage("Service CLOSED");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotWebService::tryToConnect()
{
    SkCli *cli = skApp->appCli();

    SkString user =  cli->value("--user").toString();
    SkString passwd;

    if (cli->isUsed("--password"))
        passwd =  cli->value("--password").toString();

    else
    {
        cout << "Password: "; cout.flush();
        cin >> passwd;
    }

    setObjectName(user.c_str());

    SkString robotAddress;

    if (cli->isUsed("--flow-url"))
        robotAddress =  cli->value("--flow-url").toString();

    else
    {
        AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
        robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");
    }

    bool ok = false;

    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address [$ROBOT_ADDRESS]: " << robotAddress);
        ok = localConnect(robotAddress.c_str());
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address [$ROBOT_ADDRESS]: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        int port = 9000;

        if (robotAddressParsed.count() == 2)
            port = robotAddressParsed.last().toInt();

        ok = tcpConnect(robotAddressParsed.first().c_str(), port);
    }

    if (ok)
        Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);

    //skApp->setupSharedComponent(objectName(), this);
    //async->setWorker(this);

    AssertKiller(!login(user.c_str(), passwd.c_str()));
    return ok;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotWebService::startService()
{
    SkCli *cli = skApp->appCli();

    SkString wwwPath = cli->value("--http-www-path").toString();
    wwwPath = SkFsUtils::adjustPathEndSeparator(wwwPath.c_str());

    if (!SkFsUtils::exists(wwwPath.c_str()) || !SkFsUtils::isDir(wwwPath.c_str()) || !SkFsUtils::isReadable(wwwPath.c_str()))
    {
        ObjectError("Web RootDirectory is NOT valid: " << wwwPath);
        return false;
    }

    bool ssl = cli->value("--http-ssl").toBool();
    SkString sslCertificate;
    SkString sslKey;

    if (ssl)
    {
        sslCertificate = cli->value("--http-ssl-cert").toString();

        if (sslCertificate.isEmpty())
        {
            ObjectError("SSL Certificate CANNOT be EMPTY");
            return false;
        }

        else if (!SkFsUtils::exists(sslCertificate.c_str()) || !SkFsUtils::isFile(sslCertificate.c_str()) || !SkFsUtils::isReadable(sslCertificate.c_str()))
        {
            ObjectError("SSL Certificate file is NOT valid");
            return false;
        }

        sslKey = cli->value("--http-ssl-key").toString();

        if (sslKey.isEmpty())
        {
            ObjectError("SSL key CANNOT be EMPTY");
            return false;
        }

        else if (!SkFsUtils::exists(sslKey.c_str()) || !SkFsUtils::isFile(sslKey.c_str()) || !SkFsUtils::isReadable(sslKey.c_str()))
        {
            ObjectError("SSL key file is NOT valid");
            return false;
        }
    }

    service = new SkHttpService(this);
    service->setObjectName(this, "Service");
    service->setup(objectName(),
                   ssl,
                   cli->value("--http-max-header").toUInt64(),
                   cli->value("--http-idle-timeout").toUInt16());

    if (ssl)
        service->sslSvr()->initSslCtx(sslCertificate.c_str(), sslKey.c_str());

    SkFlowSync *sync = buildSyncClient();
    AssertKiller(!sync);
    sync->setCurrentDbName("Main");
    SkVariant v;
    sync->getVariable("appName", v);
    svrName = v.toString();
    sync->close();
    sync->destroyLater();

    frontendMp = new WebFrontendMountpoint(service);

    if (!frontendMp->setup("/frontend-controller", true) || !frontendMp->init(svrName.c_str(), this) || !service->addMountPoint(frontendMp))
    {
        sync->close();
        sync->destroyLater();

        return false;
    }

    wwwMp = new SkFsMountPoint(service);

    if (!wwwMp->setup("/", true) || !wwwMp->init(wwwPath.c_str()) || !service->addMountPoint(wwwMp))
        return false;

    homeAppMp = new SkGenericMountPoint(service);
    homeAppMp->setPathAsExpandable(true);

    if (!homeAppMp->setup("/app", true) || !service->addMountPoint(homeAppMp))
        return false;

    Attach(homeAppMp, accepted, frontendMp, appPageAccepted, SkDirect);

    pairsMp = new SkGenericMountPoint(service);

    if (!pairsMp->setup("/pairs", true) || !service->addMountPoint(pairsMp))
        return false;

    Attach(pairsMp, accepted, this, onPairsPageAccepted, SkDirect);

    SkString listenAddress = cli->value("--http-addr").toString();
    UShort listenPort = cli->value("--http-port").toUInt16();
    UShort maxQueuedConnections = cli->value("--http-queued-connection").toUInt16();

    if (!service->start(listenAddress.c_str(), listenPort, maxQueuedConnections))
        return false;

    SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *itr = channelsIterator();

    while(itr->next())
    {
        SkFlowChanID chanID = itr->item().key();
        SkFlowChannel *ch = itr->item().value();

        if (ch->chan_t != StreamingChannel)
            continue;

        onChannelAdded(chanID);
    }

    delete itr;

    if (ssl)
        ObjectMessage("Service OPEN (with SSL support): " << listenAddress << ":" << listenPort);
    else
        ObjectMessage("Service OPEN (without SSL support): " << listenAddress << ":" << listenPort);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotWebService::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (isInternalChannel(ch->name.c_str()))
    {
        subscribeChannel(ch->name.c_str(), chanID);
        return;
    }

    ObjectMessage("StreamingChannel ADDED: " << ch->name << " [ChanID: " << ch->chanID << "]");

    FlowRedistrMP *r = new FlowRedistrMP;

    //HTTP section will be destroyed from http-service through the direct signal trigger removedChannel(..)
    r->mpPath = "/";
    r->mpPath.append(svrName);
    r->mpPath.append("/");
    r->mpPath.append(ch->name);

    SkRedistrMountPoint *redistrMp = nullptr;

    if (ch->mime == SkMimeType::getMimeType("jpeg"))
    {
        r->mpPath.append(".multipart");

        r->rawRedistr = nullptr;
        r->partRedistr = new SkPartRedistrMountPoint(this);

        if (!r->partRedistr->setup(r->mpPath.c_str(), true)
            || !r->partRedistr->init(svrName.c_str(), ch->mime.c_str()))
        {
            r->partRedistr->destroyLater();
            delete r;
            return;
        }

        redistrMp = r->partRedistr;
    }

    else
    {
        r->mpPath.append(".raw");

        r->partRedistr = nullptr;
        r->rawRedistr = new SkRawRedistrMountPoint(this);

        SkDataBuffer *hdr = nullptr;

        if (ch->hasHeader)
            hdr = &ch->header;

        if (!r->rawRedistr->setup(r->mpPath.c_str(), true)
            || !r->rawRedistr->init(svrName.c_str(), ch->mime.c_str(), hdr))
        {
            r->rawRedistr->destroyLater();
            delete r;
            return;
        }

        redistrMp = r->rawRedistr;
    }

    SkDeviceRedistr *redistr = redistrMp->getRedistr();
    redistr->setProperty("ChanID", chanID);
    redistr->setProperty("ChanName", ch->name.c_str());

    Attach(redistr, targetAdded, this, onStreamTargetAdded, SkQueued);
    Attach(redistr, targetDeleted, this, onStreamTargetRemoved, SkQueued);

    redistrs[chanID] = r;
    service->addMountPoint(redistrMp);
}

void RobotWebService::onChannelRemoved(SkFlowChanID chanID)
{
    if (!redistrs.contains(chanID))
        return;

    FlowRedistrMP *r = redistrs[chanID];
    redistrs.remove(chanID);

    service->delMountPoint(r->mpPath.c_str());
    ObjectMessage("Channel REMOVED: " << chanID << " [" << r->mpPath << "]");

    delete r;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotWebService::onChannelHeaderSetup(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    FlowRedistrMP *r = redistrs[chanID];

    if (r->rawRedistr)
    {
        SkDataBuffer &b = r->rawRedistr->getRedistr()->getHeader();
        ULong sz = b.size();
        r->rawRedistr->setHeader(&ch->header);
        ObjectMessage("Channel HEADER modified [" << sz << " -> " << r->rawRedistr->getRedistr()->getHeader().size() << " B]: " << ch->name);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotWebService::onChannelPublishStartRequest(SkFlowChanID)
{}

void RobotWebService::onChannelPublishStopRequest(SkFlowChanID)
{}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotWebService, fastTick)
{
    SilentSlotArgsWarning();

    if (!isConnected())
        return;

    while(nextData())
        onFlowDataCome(getCurrentData());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotWebService, onStreamTargetAdded)
{
    SilentSlotArgsWarning();

    SkDeviceRedistr *r = dynamic_cast<SkDeviceRedistr *>(referer);

    SkFlowChanID chanID = r->property("ChanID").toUInt16();
    SkString chanName = r->property("ChanName").toString();

    if (r->count() == 1/* && !isInternalChannel(chanName.c_str())*/)//  chanName != "Pairs")
    {
        ObjectWarning("Enabling redistr [chanID: " << chanID << "; name: " << chanName << "]: " << redistrs[chanID]->mpPath);
        subscribeChannel(chanName.c_str(), chanID);
    }
}

SlotImpl(RobotWebService, onStreamTargetRemoved)
{
    SilentSlotArgsWarning();

    SkDeviceRedistr *r = dynamic_cast<SkDeviceRedistr *>(referer);

    if (!r)
    {
        ObjectError("Redistr object is NULL");
        return;
    }

    SkFlowChanID chanID = r->property("ChanID").toUInt16();
    SkString chanName = r->property("ChanName").toString();

    if (r->count() == 0 /*&& !isInternalChannel(chanName.c_str())*/)// chanName != "Pairs")
    {
        ObjectWarning("Disabling redistr [chanID: " << chanID << "; name: " << chanName << "]: " << redistrs[chanID]->mpPath);
        unsubscribeChannel(chanID);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotWebService::isInternalChannel(CStr *chanName)
{
    return SkString::compare(chanName, "Pairs");
}

void RobotWebService::onFlowDataCome(SkFlowChannelData &chData)
{
    SkFlowChannel *ch = channel(chData.chanID);

    if (!ch)
        return;

    if (redistrs.contains(chData.chanID))
    {
        FlowRedistrMP *r = redistrs[chData.chanID];

        if (r->partRedistr)
            r->partRedistr->send(chData.data.data(), chData.data.size());

        else if(r->rawRedistr)
            r->rawRedistr->send(chData.data.data(), chData.data.size());

        return;
    }

    //

    if (ch->name == "Pairs")
    {
        SkString json;
        json.append(chData.data.data(), chData.data.size());

        SkVariant v;

        if (!v.fromJson(SkArrayCast::toCStr(json.c_str())))
        {
            ObjectError("Pair json is NOT valid: " << json);
            return;
        }

        SkVariantVector l;
        v.copyToList(l);

        SkString cmd = l.first().toString();
        SkString dbName = l.last().toString();
        SkString key = l[1].toString();
        SkVariant &val = l[2];

        frontendMp->dbPairChanged(cmd.c_str(), dbName.c_str(), key.c_str(), val);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  service pages

SlotImpl(RobotWebService, onPairsPageAccepted)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);

    if (!httpSck->url().getQueryMap().contains("db"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        return;
    }

    SkString dbName = httpSck->url().getQueryMap()["db"].toString();

    SkFlowSync *sync = buildSyncClient();
    AssertKiller(!sync);

    if (dbName != "Main" && !sync->existsOptionalPairDb(dbName.c_str()))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
        sync->close();
        sync->destroyLater();
        return;
    }

    sync->setCurrentDbName(dbName.c_str());

    if (!httpSck->url().getQueryMap().contains("cmd"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        sync->close();
        sync->destroyLater();
        return;
    }

    SkString cmd = httpSck->url().getQueryMap()["cmd"].toString();

    SkString json;

    if (cmd == "keys")
    {
        SkStringList keys;
        sync->variablesKeys(keys);

        SkVariant v = keys;

        if (!v.toJson(json, true))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
            sync->close();
            sync->destroyLater();
            return;
        }
    }

    else
    {
        SkString key;

        if (!httpSck->url().getQueryMap().contains("key"))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
            sync->close();
            sync->destroyLater();
            return;
        }

        key = httpSck->url().getQueryMap()["key"].toString();

        if (!sync->existsVariable(key.c_str()))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
            sync->close();
            sync->destroyLater();
            return;
        }

        if (cmd == "get")
        {
            SkVariant v;

            if (!sync->getVariable(key.c_str(), v) || !v.toJson(json, true))
            {
                service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
                sync->close();
                sync->destroyLater();
                return;
            }
        }

        else if (cmd == "del")
        {
            sync->delVariable(key.c_str());

            httpSck->send(SkHttpStatusCode::Ok);
            sync->close();
            sync->destroyLater();
            return;
        }
    }

    httpSck->send(json, "application/json", SkHttpStatusCode::Ok);
    sync->close();
    sync->destroyLater();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
