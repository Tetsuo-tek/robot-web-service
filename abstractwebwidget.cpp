#include "abstractwebwidget.h"
#include "webwidgetcontainer.h"

#include "Core/Scripting/SkCodeTemplate/sktemplatemanager.h"

AbstractConstructorImpl(AbstractWebWidget, SkObject)
{
    t = WebWidgetType::WT_NOTYPE;
    interval = 0.;
}

void AbstractWebWidget::setLabel(CStr *label)
{
    lbl = label;
}

void AbstractWebWidget::setHtmlTemplate(CStr *htmlTemplatePath, SkArgsMap &customize)
{
    html.filePath = htmlTemplatePath;
    html.customize = customize;
}

void AbstractWebWidget::setInitJsTemplate(CStr *jsTemplatePath, SkArgsMap &customize)
{
    initJs.filePath = jsTemplatePath;
    initJs.customize = customize;
}

void AbstractWebWidget::setTickJsTemplate(CStr *jsTemplatePath, SkArgsMap &customize, double intervalSeconds)
{
    tickJs.filePath = jsTemplatePath;
    tickJs.customize = customize;
    interval = intervalSeconds;
}

bool AbstractWebWidget::hasHtmlTemplate()
{
    return !html.filePath.isEmpty();
}

WebWidgetTemplate &AbstractWebWidget::htmlTemplate()
{
    return html;
}

SkHtmlTemplate *AbstractWebWidget::buildHtml(SkTemplateManager *manager)
{
    SkHtmlTemplate *item = manager->newHtml(html.filePath.c_str());
    item->setVariable("LABEL", lbl.c_str());
    item->setVariable("ID", wid.c_str());

    SkList<SkHtmlTemplate *> childrenItems;

    if (t == WebWidgetType::WT_CONTAINER)
    {
        SkList<AbstractWebWidget *> &children = dynamic_cast<WebWidgetContainer *>(this)->childrenWidgets();

        if (!children.isEmpty())
        {
            SkAbstractListIterator<AbstractWebWidget *> *itr = children.iterator();

            while(itr->next())
            {
                AbstractWebWidget *w = itr->item();

                if (w->hasHtmlTemplate())
                    childrenItems << w->buildHtml(manager);
            }

            delete itr;
        }

        SkString s(children.count());
        item->setVariable("CHILDREN_COUNT", s.c_str());

        //$#CONTENT#$ IS MANDATORY FOR ANY html-container TEMPLATE
        item->setBlocks("CONTENT", childrenItems);
    }

    WebWidgetContainer *wc = container();

    if (wc)
    {
        SkString s(wc->childrenWidgets().indexOf(this));
        item->setVariable("CHILD_INDEX", s.c_str());
        item->setVariable("PARENT_ID", wc->id());
    }

    if (!html.customize.isEmpty())
        item->setCustomization(html.customize);

    return item;
}

bool AbstractWebWidget::hasInitJsTemplate()
{
    return !initJs.filePath.isEmpty();
}

WebWidgetTemplate &AbstractWebWidget::initJsTemplate()
{
    return initJs;
}

bool AbstractWebWidget::buildInitJs(SkTemplateManager *manager, SkString &outputCode)
{
    SkJsTemplate *item = nullptr;
    WebWidgetContainer *wc = container();

    if (hasInitJsTemplate())
    {
        item = manager->newJs(initJs.filePath.c_str());
        item->setVariable("LABEL", lbl.c_str());
        item->setVariable("ID", wid.c_str());

        if (wc)
        {
            SkString s(wc->childrenWidgets().indexOf(this));
            item->setVariable("CHILD_INDEX", s.c_str());
            item->setVariable("PARENT_ID", wc->id());
        }
    }

    if (t == WebWidgetType::WT_CONTAINER)
    {
        SkList<AbstractWebWidget *> &children = dynamic_cast<WebWidgetContainer *>(this)->childrenWidgets();

        if (item)
        {
            SkString s(children.count());
            item->setVariable("CHILDREN_COUNT", s.c_str());

            if (!initJs.customize.isEmpty())
                item->setCustomization(initJs.customize);

            if (!item->build(outputCode))
                return false;
        }

        if (!children.isEmpty())
        {
            SkAbstractListIterator<AbstractWebWidget *> *itr = children.iterator();

            while(itr->next())
            {
                AbstractWebWidget *w = itr->item();

                if (!w->buildInitJs(manager, outputCode))
                {
                    ObjectError("Cannot build InitJs template for child: " << w->initJs.filePath);
                    delete itr;
                    return false;
                }
            }

            delete itr;
        }
    }

    else if (item)
    {
        if (!initJs.customize.isEmpty())
            item->setCustomization(initJs.customize);

        if (!item->build(outputCode))
            return false;
    }

    return true;
}

double AbstractWebWidget::tickTimeInterval()
{
    return interval;
}

bool AbstractWebWidget::hasTickJsTemplate()
{
    return !tickJs.filePath.isEmpty();
}

WebWidgetTemplate &AbstractWebWidget::tickJsTemplate()
{
    return tickJs;
}

bool AbstractWebWidget::buildTickJs(SkTemplateManager *manager, SkString &outputCode)
{
    SkJsTemplate *item = nullptr;
    WebWidgetContainer *wc = container();

    if (hasTickJsTemplate() && tickChrono.stop() >= interval)
    {
        item = manager->newJs(tickJs.filePath.c_str());
        item->setVariable("ID", wid.c_str());

        if (wc)
        {
            SkString s(wc->childrenWidgets().indexOf(this));
            item->setVariable("CHILD_INDEX", s.c_str());
            item->setVariable("PARENT_ID", wc->id());
        }

        tickChrono.start();
    }

    if (t == WebWidgetType::WT_CONTAINER)
    {
        SkList<AbstractWebWidget *> &children = dynamic_cast<WebWidgetContainer *>(this)->childrenWidgets();

        if (item)
        {
            SkString s(children.count());
            item->setVariable("CHILDREN_COUNT", s.c_str());

            if (!tickJs.customize.isEmpty())
                item->setCustomization(tickJs.customize);

            if (!item->build(outputCode))
                return false;
        }

        if (!children.isEmpty())
        {
            SkAbstractListIterator<AbstractWebWidget *> *itr = children.iterator();

            while(itr->next())
            {
                AbstractWebWidget *w = itr->item();

                if (!w->buildTickJs(manager, outputCode))
                {
                    ObjectError("Cannot build InitJs template for child: " << w->tickJs.filePath);
                    delete itr;
                    return false;
                }
            }

            delete itr;
        }
    }

    else if (item)
    {
        if (!tickJs.customize.isEmpty())
            item->setCustomization(tickJs.customize);

        if (!item->build(outputCode))
            return false;
    }

    return true;
}

CStr *AbstractWebWidget::label()
{
    return lbl.c_str();
}

CStr *AbstractWebWidget::id()
{
    return wid.c_str();
}

WebWidgetContainer *AbstractWebWidget::container()
{
    if (!parent())
        return nullptr;

    return dynamic_cast<WebWidgetContainer *>(parent());
}

WebWidgetType AbstractWebWidget::widgetType()
{
    return t;
}
