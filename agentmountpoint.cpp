#include "agentmountpoint.h"

#if defined(UMBA)
#include <Core/App/skeventloop.h>
#include <Core/Containers/skarraycast.h>
#include <Core/System/Network/TCP/HTTP/Server/skhttpservice.h>
#include <Core/System/Network/TCP/HTTP/skwebsocket.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WebAgentConnection, SkFlowAsync)
{
    sck = nullptr;

    agentSpeakingPcmChanName = "Agent.Speech.Pcm";
    agentSpeakingPcmChan = nullptr;

    faceBoxesChanName = "FaceDetect.DetectionBoxes";
    faceBoxesChan = nullptr;

    faceIDChanName = "FaceRecognize.FaceID";
    faceIDChan = nullptr;

    camChan = -1;
    micChan = -1;

    SlotSet(onTextReadyRead);
    SlotSet(onDataReadyRead);
    SlotSet(onDisconnected);

    SlotSet(onFastTick);
    SlotSet(onSlowTick);
    SlotSet(onOneSecondTick);

    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkQueued);
    Attach(eventLoop()->slowZone_SIG, pulse, this, onSlowTick, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecondTick, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebAgentConnection::setup(SkWebSocket *socket)
{
    sck = socket;
    sck->setSendingMode(WSM_TEXT);

    Attach(sck, textReadyRead, this, onTextReadyRead, SkQueued);
    Attach(sck, binaryReadyRead, this, onDataReadyRead, SkQueued);
    Attach(sck, disconnected, this, onDisconnected, SkDirect);

    addStreamingChannel(micChan, FT_AUDIO_DATA, T_INT16, "Short");
    addStreamingChannel(camChan, FT_VIDEO_DATA, T_BYTEARRAY, "JPeg", "image/jpeg");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <zlib.h>
SlotImpl(WebAgentConnection, onTextReadyRead)
{
    SilentSlotArgsWarning();

    SkString b;
    sck->read(b);
    cout << "-> " << b << "\n";
}

SlotImpl(WebAgentConnection, onDataReadyRead)
{
    SilentSlotArgsWarning();

    if (!sck->isConnected())
        return;

    SkDataBuffer b;
    sck->read(&b);

    if (SkString::compare(SkArrayCast::toChar(b.toVoid()), " CAM", 4))
    {
        //cout << "CAM " << b.size() << "\n";
        publish(camChan, &b.data()[4], b.size()-4);
    }

    else if (SkString::compare(SkArrayCast::toChar(b.toVoid()), " MIC", 4))
    {
        //cout << "MIC " << b.size() << "\n";
        publish(micChan, &b.data()[4], b.size()-4);
    }
}

SlotImpl(WebAgentConnection, onDisconnected)
{
    SilentSlotArgsWarning();

    if (camChan > -1)
        removeChannel(camChan);

    if (micChan > -1)
        removeChannel(micChan);

    sck = nullptr;
    ObjectMessage("REMOVED socket");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebAgentConnection::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (ch->name == agentSpeakingPcmChanName)
    {
        agentSpeakingPcmChan = ch;
        subscribeChannel(ch);
    }

    else if (ch->name == faceBoxesChanName)
    {
        faceBoxesChan = ch;
        subscribeChannel(ch);
    }

    else if (ch->name == faceIDChanName)
    {
        faceIDChan = ch;
        subscribeChannel(ch);
    }
}

void WebAgentConnection::onChannelRemoved(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (ch == agentSpeakingPcmChan)
        agentSpeakingPcmChan = nullptr;

    else if (ch->name == faceBoxesChanName)
        faceBoxesChan = nullptr;

    else if (ch->name == faceIDChanName)
        faceIDChan = nullptr;
}

void WebAgentConnection::onFlowDataCome(SkFlowChanID chanID, void *data, UInt sz)
{
    if (agentSpeakingPcmChan && chanID == agentSpeakingPcmChan->chanID)
    {
        sck->setSendingMode(WSM_BINARY);
        sck->write(SkArrayCast::toChar(data), sz);

        ObjectWarning("Sent PCM chunk: " << sz << " B");
    }

    else if (faceBoxesChan && chanID == faceBoxesChan->chanID)
    {
        SkString jsonInput(SkArrayCast::toChar(data), sz);
        SkVariant faceBoxesVal;
        faceBoxesVal.fromJson(jsonInput.c_str());

        SkString json;
        SkArgsMap cmdMap;
        cmdMap["cmd"] = "FACE_BOXES";
        cmdMap["data"] = faceBoxesVal;
        cmdMap.toString(json);

        sck->setSendingMode(WSM_TEXT);
        sck->write(json);

        ObjectWarning("Sent FaceBoxes: " << json);
    }

    else if (faceIDChan && chanID == faceIDChan->chanID)
    {
        /*SkString jsonInput(SkArrayCast::toChar(data), sz);
        SkVariant faceIDVal;
        faceIDVal.fromJson(jsonInput.c_str());

        SkString json;
        SkArgsMap cmdMap;
        cmdMap["cmd"] = "FACE_ID";
        cmdMap["data"] = faceIDVal;
        cmdMap.toString(json);

        sck->setSendingMode(WSM_TEXT);
        sck->write(json);

        ObjectWarning("Sent FaceID: " << json);*/
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WebAgentConnection, onFastTick)
{
    SilentSlotArgsWarning();

    if (!isConnected())
        return;

    //

    while(nextData())
    {
        SkFlowChannelData &d = getCurrentData();

        if (!d.data.isEmpty())
            onFlowDataCome(d.chanID, d.data.toVoid(), d.data.size());
    }
}

SlotImpl(WebAgentConnection, onSlowTick)
{
    SilentSlotArgsWarning();

    if (!sck)
        return;

    /*SkArgsMap cmdMap;
    cmdMap["cmd"] = "RENDER";

    SkString json;
    cmdMap.toString(json);
    sck->write(json);*/
}

SlotImpl(WebAgentConnection, onOneSecondTick)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WebAgentMountpoint, SkGenericMountPoint)
{
    conn = nullptr;

    SlotSet(onUserAccepted);
    SlotSet(onUserDisconnected);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WebAgentMountpoint::init(CStr *serverName, CStr *fsPath, CStr *fsUser, CStr *fsToken)
{
    svrName = serverName;
    svrPath = fsPath;
    userName = fsUser;
    token = fsToken;

    Attach(this, accepted, this, onUserAccepted, SkQueued);
    return true;
}

void WebAgentMountpoint::close()
{
    Detach(this, accepted, this, onUserAccepted);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WebAgentMountpoint, onUserAccepted)
{
    SilentSlotArgsWarning();

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);

    if (conn)
    {
        ObjectWarning("User is ALREADY connected; killing: " << wsSck->objectName());
        wsSck->disconnect();
        wsSck->destroyLater();
        return;
    }

    Attach(wsSck, disconnected, this, onUserDisconnected, SkDirect);

    conn = new WebAgentConnection(this);
    conn->setObjectName(this, "WebAgentInterface");

    if (!conn->localConnect(svrPath.c_str()) || !conn->login(userName.c_str(), token.c_str()))
    {
        conn->destroyLater();
        conn = nullptr;
        return;
    }

    conn->setCurrentDbName(userName.c_str());
    conn->setup(wsSck);

    ObjectMessage("ADDED Agent user-connection: " << conn->objectName());
}

SlotImpl(WebAgentMountpoint, onUserDisconnected)
{
    SilentSlotArgsWarning();

    if (conn->isConnected())
        conn->close();

    conn->destroyLater();
    ObjectMessage("REMOVED Agent user-connection: " << conn->objectName());

    conn = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#endif
