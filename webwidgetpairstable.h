#ifndef WEBWIDGETPAIRSTABLE_H
#define WEBWIDGETPAIRSTABLE_H

#include "webwidgetcontainer.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class WebWidgetPairsTable extends WebWidgetContainer
{
    public:
        Constructor(WebWidgetPairsTable, WebWidgetContainer);

        void setPair(CStr *pairDbName, CStr *pairKey, CStr *udm=nullptr);
        void build(SkArgsMap &pairsMap);

        CStr *pairsDatabaseName();
        CStr *pairKey();

    private:
        SkString k;
        SkString dbName;
        SkString u;

        WebWidgetContainer *pairs;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // WEBWIDGETPAIRSTABLE_H
