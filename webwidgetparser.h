#ifndef WEBWIDGETPARSER_H
#define WEBWIDGETPARSER_H

#include "webwidgetpairstable.h"
#include "webwidgetform.h"

#include <Core/System/Filesystem/skfsutils.h>
#include <Core/Containers/sktreemap.h>
#include <Core/Scripting/SkCodeTemplate/sktemplatemodeler.h>

class SkFlowAsync;
class SkFlowSync;
class WebPageWidgets;

class WebWidgetParser extends SkTemplateModeler
{
    SkArgsMap widgets;
    SkFlowSync *sync;

    public:
        WebWidgetParser();

        bool parse(CStr *filePath, SkFlowAsync *flow);

    protected:
        SkPathInfo wuiPathInfo;

        virtual void addWidgetContainer(WebWidgetContainer *)   = 0;
        virtual void addWidget(WebWidgetValue *, SkFlowSync *)  = 0;
        virtual void addWidgetForm(WebWidgetForm *)             = 0;
        virtual void onFileEvaluation(SkFileInfo *)             = 0;

    private:
        bool parseAbstractWidgetsList(SkVariant &v, WebWidgetContainer *container);
        bool parseWidgetContainer(SkArgsMap &m, WebWidgetContainer *container);
        bool parseWidget(SkArgsMap &m, WebWidgetContainer *container);
        bool parseWidgetPairs(SkArgsMap &m, WebWidgetContainer *container);
        bool parseWidgetForm(SkArgsMap &m, WebWidgetContainer *container);
        bool evaluateFile(CStr *filePath, SkString &output);
        bool onKeyNotFound(CStr *k, SkString &replaced) override;
};

#endif // WEBWIDGETPARSER_H
