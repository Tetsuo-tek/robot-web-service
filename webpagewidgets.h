#ifndef WEBPAGEWIDGETS_H
#define WEBPAGEWIDGETS_H

#include "webwidgetparser.h"
#include "webwidgetcontainer.h"
#include <Core/Scripting/SkCodeTemplate/sktemplatemanager.h>

class SkWebSocket;
class SkTemplateManager;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct  WebFrontendSocket
{
    SkWebSocket *sck;
    WebPageWidgets *currentPage;
};

class WebPageWidgets extends WebWidgetParser
{
    SkFlowAsync *async;

    SkString titleText;
    SkString directory;
    SkList<SkFileInfo *> wuiFileInfos;
    SkList<WebWidgetContainer *> containers;
    SkTreeMap<SkString, WebWidgetValue *> pairSourceWidgets;//wID, W
    SkTreeMap<SkString, WebWidgetForm *> formWidgets;//wID, W
    SkTreeMap<SkString, WebFrontendSocket *> frontendClients;
    SkTemplateManager *templateManager;

    public:
        WebPageWidgets();

        bool init(CStr *dirPath, SkFlowAsync *flow, SkTemplateManager *templates);
        void close();

        void addFrontend(WebFrontendSocket *frontend, bool initialize=true);
        void delFrontend(WebFrontendSocket *frontend);

        SkTreeMap<SkString, WebFrontendSocket *> &frontends();

        CStr *title();

        WebWidgetValue *pairSource(CStr *dbName, CStr *k);

        bool htmlCode(SkString &html);
        bool initCode(SkString &code);
        bool tickCode(SkString &html);

        void inject(CStr *code);

        void injectPairRunCodeToAll(CStr *dbName, CStr *k, SkVariant &v);

        void tick();
        void updateCheck();

    private:
        bool buildPairRunCode(CStr *dbName, CStr *k, SkVariant &v, SkString &code);
        void buildAllDbPairsRunCode(CStr *dbName, SkString &code, SkFlowSync *sync);

        void addWidgetContainer(WebWidgetContainer *wc)                     override;
        void addWidget(WebWidgetValue *w, SkFlowSync *sync)                 override;
        void addWidgetForm(WebWidgetForm *f)                                override;
        void onFileEvaluation(SkFileInfo *wuiFileInfo)                      override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // WEBPAGEWIDGETS_H
