#ifndef ROBOTWEBSERVICE_H
#define ROBOTWEBSERVICE_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/App/skapp.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkHttpService;
class SkGenericMountPoint;
class SkFsMountPoint;
class WebFrontendMountpoint;
class SkRawRedistrMountPoint;
class SkPartRedistrMountPoint;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct FlowRedistrMP
{
    SkFlowChanID chanID;
    SkString mpPath;
    SkRawRedistrMountPoint *rawRedistr;
    SkPartRedistrMountPoint *partRedistr;
};

class RobotWebService extends SkFlowAsync
{
    SkString svrName;
    SkHttpService *service;

    SkFsMountPoint *wwwMp;

    SkGenericMountPoint *homeAppMp;
    SkGenericMountPoint *pairsMp;
    WebFrontendMountpoint *frontendMp;

    SkTreeMap<SkFlowChanID, FlowRedistrMP *> redistrs;

    public:
        Constructor(RobotWebService, SkFlowAsync);

        Slot(init);
        Slot(quit);

        Slot(onPairsPageAccepted);
        Slot(onStreamTargetAdded);
        Slot(onStreamTargetRemoved);

        Slot(fastTick);

        Slot(onAppExitRequest);

    private:
        bool tryToConnect();
        bool startService();

        void onChannelAdded(SkFlowChanID)                               override;
        void onChannelRemoved(SkFlowChanID)                             override;
        void onChannelHeaderSetup(SkFlowChanID)                         override;
        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;

        void onFlowDataCome(SkFlowChannelData &chData);

        bool isInternalChannel(CStr *chanName);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ROBOTWEBSERVICE_H
