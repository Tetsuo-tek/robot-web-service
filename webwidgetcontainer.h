#ifndef WEBWIDGETCONTAINER_H
#define WEBWIDGETCONTAINER_H

#include "webwidgetvalue.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class WebWidgetContainer extends AbstractWebWidget
{
    public:
        Constructor(WebWidgetContainer, AbstractWebWidget);

        void setID(CStr *id);

        void add(AbstractWebWidget *w);
        void remove(AbstractWebWidget *w);

        SkList<AbstractWebWidget *> &childrenWidgets();

    private:
        SkList<AbstractWebWidget *> widgets;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
#endif // WEBWIDGETCONTAINER_H
