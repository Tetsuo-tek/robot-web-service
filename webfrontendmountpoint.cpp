#include "webfrontendmountpoint.h"

#include <Core/System/Network/TCP/HTTP/Server/skhttpservice.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>

#include <Core/App/skapp.h>
#include <Core/Object/skabstractworkerobject.h>
#include <Core/System/Filesystem/skfileinfoslist.h>
#include <Core/System/skdeviceredistr.h>
#include <Core/System/Network/TCP/HTTP/skwebsocket.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WebFrontendMountpoint, SkGenericMountPoint)
{
    async = nullptr;
    service = nullptr;

    SlotSet(onFrontendAccepted);
    SlotSet(onFrontendTextReadyRead);
    SlotSet(onFrontendDataReadyRead);
    SlotSet(onFrontendDisconnected);

    SlotSet(onWrkEvaluatedCmd);

    SlotSet(onFastTick);
    SlotSet(onOneSecondTick);

    SlotSet(appPageAccepted);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WebFrontendMountpoint::init(CStr *serverName, SkFlowAsync *flow)
{
    async = flow;
    svrName = serverName;
    service = dynamic_cast<SkHttpService *>(parent());

    setObjectName(service, "FrontendMP");
    templateManager.setObjectName(this, "templatesManager");

    if (!templateManager.init("templates/"))
    {
        ObjectWarning("Templates path NOT found: templates/");
        return false;
    }

    AssertKiller(!initPages());

    Attach(this, accepted, this, onFrontendAccepted, SkQueued);
    Attach(eventLoop()->fastZone_SIG, pulse, this, onFastTick, SkQueued);
    Attach(eventLoop()->oneSecZone_SIG, pulse, this, onOneSecondTick, SkQueued);

    return true;
}

void WebFrontendMountpoint::close()
{
    {
        SkBinaryTreeVisit<SkString, WebPageWidgets *> *itr = pages.iterator();

        while(itr->next())
        {
            WebPageWidgets *page = itr->item().value();
            page->close();
            delete page;
        }

        delete itr;
        pages.clear();
    }

    templateManager.clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WebFrontendMountpoint::initPages()
{
    SkFileInfosList *l = new SkFileInfosList(this);
    SkFsUtils::ls("pages.d/", l, false);

    l->open();

    while(l->next())
        if (l->currentPathIsDir() && l->currentPath().endsWith(".page"))
        {
            WebPageWidgets *page  = new WebPageWidgets;
            page->setObjectName(this, "WuiPage");

            if (page->init(l->currentPath().c_str(), async, &templateManager))
            {
                pages[page->title()] = page;
                ObjectMessage("Built wui page: " << page->title());
            }

            else
                delete page;
        }

    l->close();
    l->destroyLater();

    if (!pages.contains("Main"))
    {
        ObjectError("Main.page is MANDATORY");
        return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebFrontendMountpoint::inject(CStr *code, SkWebSocket *sck)
{
    SkArgsMap m;
    m["command"] = "EVAL";
    m["code"] = code;

    SkString json;
    m.toString(json);

    sck->write(json.c_str(), json.size());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkHtmlTemplate *WebFrontendMountpoint::buildMainPage(CStr *title, CStr *titleRef, CStr *footer)
{
    SkArgsMap menuConfig;

    if (!menuConfig.fromFile("pages.d/menu.json"))
    {
        ObjectError("Menu configuration file NOT found: pages.d/menu.json");
        return nullptr;
    }

    SkHtmlTemplate *page = templateManager.newHtml("index.html");
    page->setVariable("TITLE", title);
    page->setVariable("LOGO_IMG", "/Protozoic-LOGO.png");
    page->setVariable("TITLE_REF", titleRef);

    SkString pagesMenu;
    SkList<SkHtmlTemplate *> l;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = menuConfig.iterator();

    while(itr->next())
    {
        SkString &subMenuName = itr->item().key();
        SkStringList pageTitles;
        itr->item().value().copyToStringList(pageTitles);

        SkHtmlTemplate *dropdownMenu = templateManager.newHtml("menu/dropdown-menu.html");
        dropdownMenu->setVariable("MENU_NAME", subMenuName.c_str());

        SkString itemList;

        for(uint64_t i=0; i<pageTitles.count(); i++)
        {
            SkString &pageName = pageTitles[i];

            if (pageName == "Main")
                continue;

            SkString refPath = titleRef;
            refPath.append("/");
            refPath.append(pageName);

            SkHtmlTemplate *menuItem = templateManager.newHtml("menu/menu-item.html");
            menuItem->setVariable("ITEM_REF", refPath.c_str());
            menuItem->setVariable("ITEM_NAME", pageName.c_str());

            if (!menuItem->build(itemList))
            {
                delete itr;
                return nullptr;
            }
        }

        dropdownMenu->setBlock("ITEMS", itemList.c_str());

        if (!dropdownMenu->build(pagesMenu))
            return nullptr;
    }

    delete itr;

    page->setBlock("MENU", pagesMenu.c_str());
    page->setBlock("FOOTER", footer);
    return page;
}

SlotImpl(WebFrontendMountpoint, appPageAccepted)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);
    SkHtmlTemplate *page = buildMainPage(svrName.c_str(), "/app", "<strong>SkRobot</strong>");

    if (!page)
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
        templateManager.clear();
        return;
    }

    SkString code;

    if (!page->build(code))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
        templateManager.clear();
        return;
    }

    templateManager.clear();
    httpSck->send(code, "text/html", SkHttpStatusCode::Ok);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WebFrontendMountpoint::buildRoutes(SkString &code)
{
    SkString routes;
    routes.append("\"/app\":{\"cmd\":\"SET_PAGE\",\"page\":\"Main\"},");

    SkBinaryTreeVisit<SkString, WebPageWidgets *> *itr = pages.iterator();

    while(itr->next())
    {
        SkString &pageName = itr->item().key();

        if (pageName == "Main")
            continue;

        SkString refPath = "/app/";
        refPath.append(pageName);

        routes.append("\"");
        routes.append(refPath);
        routes.append("\":{\"cmd\":\"SET_PAGE\",\"page\":\"");
        routes.append(pageName);
        routes.append("\"}");

        if (itr->hasNext())
            routes.append(",");
    }

    delete itr;

    SkJsTemplate *routerJs = templateManager.newJs("router.js");
    routerJs->setBlock("ROUTES", routes.c_str());

    if (!routerJs->build(code))
        return false;

    return true;
}

SlotImpl(WebFrontendMountpoint, onFrontendAccepted)
{
    SilentSlotArgsWarning();

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);
    wsSck->setSendingMode(WSM_TEXT);

    WebFrontendSocket *frontend = new WebFrontendSocket;
    frontend->currentPage = nullptr;
    frontend->sck = wsSck;
    frontends[wsSck->getKey()] = frontend;

    SkString code;

    if (!buildRoutes(code))
    {
        templateManager.clear();
        frontend->sck->disconnect();
        return;
    }

    WebFrontendMountpoint::inject(code.c_str(), frontend->sck);
    templateManager.clear();

    Attach(wsSck, textReadyRead, this, onFrontendTextReadyRead, SkDirect);
    Attach(wsSck, binaryReadyRead, this, onFrontendDataReadyRead, SkDirect);
    Attach(wsSck, disconnected, this, onFrontendDisconnected, SkDirect);

    skApp->setupSharedComponent(wsSck->getKey(), wsSck);

    ObjectMessage_EXT(wsSck, "ADDED frontend");
}

SlotImpl(WebFrontendMountpoint, onFrontendTextReadyRead)
{
    SilentSlotArgsWarning();

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);
    SkString wsKey = wsSck->getKey();

    if (!wsSck->bytesAvailable())
    {
        ObjectError_EXT(wsSck, "FRONTEND-COMMAND IS EMPTY");
        return;
    }

    SkString json;
    wsSck->read(json);

    ObjectMessage_EXT(wsSck, "Received FRONTEND-COMMAND: " << json);

    SkArgsMap cmdMap;

    if (cmdMap.fromString(json.c_str()))
    {
        SkString cmd = cmdMap["cmd"].data();

        if (cmd == "SET_PAGE")
        {
            SkString pageName = cmdMap["page"].data();

            if (pages.contains(pageName))
            {
                WebPageWidgets *p = pages[pageName];
                WebFrontendSocket *sck = frontends[wsKey];

                if (sck->currentPage)
                    sck->currentPage->delFrontend(sck);

                p->addFrontend(sck);
            }

            else
            {
                ObjectError("Wui page NOT found: " << pageName);
                wsSck->disconnect();
            }
        }

        else if (cmd == "UNSET_PAGE")
        {
            WebFrontendSocket *sck = frontends[wsKey];
            sck->currentPage->delFrontend(sck);
        }

        else if (cmd == "URL_REQ")
        {
            SkString url = cmdMap["url"].data();
            ObjectMessage_EXT(wsSck, "Loading URL: " << url);
        }

        else if (cmd == "FORM")
        {
            if (cmdMap.contains("OWNER_TYPE"))
            {
                SkString wrkName = cmdMap["OWNER_NAME"].data();
                SkString wrkType = cmdMap["OWNER_TYPE"].data();
                SkString wrkSlot = cmdMap["OWNER_SLOT"].data();

                ObjectMessage_EXT(wsSck, "Received module-command FORM [mod_T: "<< wrkType << "; name: " << wrkName << "]: " << wrkSlot);

                cmdMap.remove("cmd");
                cmdMap.remove("OWNER_TYPE");
                cmdMap.remove("OWNER_NAME");
                cmdMap.remove("OWNER_SLOT");

                //DA RIVEDERE (sostituire solo on/off se è T_BOOL, usando la stessa mappa)
                SkArgsMap m;
                SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = cmdMap.iterator();

                while(itr->next())
                {
                    SkString &s = itr->item().key();
                    SkVariant &v = itr->item().value();

                    //if (!v.toString().isEmpty())
                    {
                        if (v.toString() == "on")
                            v = true;

                        /*else if (v.toString() == "off")
                            v = false;*/

                        m[s] = v;
                    }
                }

                delete itr;
                //

                SkAbstractWorkerObject *wrk = dynamic_cast<SkAbstractWorkerObject *>(skApp->getSharedComponent(wrkName.c_str()));

                if (wrk)
                {
                    if (wrkType != wrk->typeName())
                    {
                        ObjectError_EXT(wrk, "CANNOT build WorkerTransaction, because the Worker type NOT match [" << wrkType << "]: " << wrk->typeName());
                        wsSck->disconnect();
                        return;
                    }

                    SkWorkerTransaction *t = new SkWorkerTransaction(this);
                    t->setCommand(wrk, wsSck, onWrkEvaluatedCmd_SLOT, wrkSlot.c_str(), &m);
                    t->invoke();
                }

                else
                    ObjectWarning("WorkerObject NOT found: " << wrkName << " [" << wrkType << "]");
            }

            else
            {
                ObjectError_EXT(wsSck, "OWNER_TYPE, OWNER_NAME, OWNER_SLOT are mandatory");
                wsSck->disconnect();
            }
        }

        /*else if (cmd == "OPEN_CHAN")
        {
            SkString name = cmdMap["name"].data();
            //SkString typeName = cmdMap["t"].data();
            SkString mime = cmdMap["mime"].data();
            SkString udm = cmdMap["udm"].data();

            SkFlowChanID chanID = fs->addChannel(SkVariant_T::T_BYTEARRAY, name.c_str(), mime.c_str(), udm.c_str());

            if (chanID == -1)
            {
                //SEND ERROR MESSAGE
                return;
            }
        }

        else if (cmd == "CLOSE_CHAN")
        {
            SkString name = cmdMap["name"].data();
            SkFlowRedistrChannel *ch = fs->getChannelByName(name.c_str());

            if (!ch)
            {
                //SEND ERROR MESSAGE
                return;
            }

            fs->removeChannel(ch->id());
        }*/

        else
        {
            //SEND ERROR MESSAGE
            SkString s;
            cmdMap.toString(s);
            ObjectError_EXT(wsSck, "FRONTEND-COMMAND UNKNOWN: " << s);
            wsSck->disconnect();
        }
    }
    else
    {
        //SEND ERROR MESSAGE
        ObjectError_EXT(wsSck, "FRONTEND-COMMAND IS NOT VALID: " << json);
        //wsSck->disconnect();
    }
}

SlotImpl(WebFrontendMountpoint, onFrontendDataReadyRead)
{
    SilentSlotArgsWarning();

    /*SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);

    SkFlowChanID chanID = wsSck->readInt16();
    SkFlowRedistrChannel *ch = fs->getChannelByID(chanID);

    if (!ch)
        return;

    SkDataBuffer b;
    wsSck->read(&b);

    ch->publish(b.toVoid(), b.size());*/
}

SlotImpl(WebFrontendMountpoint, onFrontendDisconnected)
{
    SilentSlotArgsWarning();

    /*if (!fs)
        return;*/

    SkWebSocket *wsSck = dynamic_cast<SkWebSocket *>(referer);
    SkString wsKey = wsSck->getKey();

    skApp->removeSharedComponent(wsKey);
    WebFrontendSocket *frontend = frontends[wsKey];

    if (frontend->currentPage)
        frontend->currentPage->delFrontend(frontend);

    frontends.remove(wsKey);
    delete frontend;

    //destroy happens on mountpoint
    ObjectMessage("REMOVED frontend [" << wsSck->objectName() << "; key: " << wsSck->getKey() << "]");

    ObjectMessage_EXT(wsSck, "FRONTEND disconnected");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WebFrontendMountpoint, onWrkEvaluatedCmd)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    SkString args;
    t->argument().toString(args);

    ObjectMessage("DEVICE Command transaction has returned back: " << t->worker()->objectName() << "::"<< t->command() << " [args: " << args << "]");

    if (t->isStillAlive())
    {
        /*if (!t->hasErrors() && t->commandSlot()->isControl())
        {
            t->destroyLater();
            return;
        }*/

        SkWuiElement *notify = SkWuiElement::wui_DIV("")
                ->attr("role", "alert");

        SkWuiElement *icon = SkWuiElement::wuiElement("", notify)
                ->setTag("svg")
                ->setClasses("bi flex-shrink-0 me-2")
                ->attr("width", "24")
                ->attr("height", "24")
                ->attr("role", "img");

        if (t->hasErrors())
        {
            notify->setClasses("alert alert-danger");
            icon->attr("aria-label", "Danger:");

            SkWuiElement::wuiElement("", icon)
                    ->setTag("use")
                    ->attr("xlink:href", "#exclamation-triangle-fill");

            SkWuiElement::wui_STRONG("", "Transaction terminated with errors", notify);

            SkWuiElement::wui_HR(notify);
            SkWuiElement::wui_P("", t->error(), notify);
        }

        else
        {
            notify->setClasses("alert alert-success");
            icon->attr("aria-label", "Success:");

            SkWuiElement::wuiElement("", icon)
                    ->setTag("use")
                    ->attr("xlink:href", "#check-circle-fill");

            SkWuiElement::wui_STRONG("", "Transaction terminated with success", notify);
        }

        if (!t->response().isEmpty())
        {
            SkWuiElement::wui_HR(notify);
            SkWuiElement *dl = SkWuiElement::wuiElement("", notify)
                    ->setTag("dl")
                    ->setClasses("row");

            SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = t->response().iterator();

            while(itr->next())
            {
                SkString &k = itr->item().key();
                SkString v = itr->item().value().toString();

                SkWuiElement::wuiElementWithText("", k.c_str(), dl)
                        ->setTag("dt")
                        ->setClasses("col-3");

                SkWuiElement::wuiElementWithText("", v.c_str(), dl)
                        ->setTag("dd")
                        ->setClasses("col-9");
            }

            delete itr;
        }

        SkXmlWriter htmlWriter;
        SkDataBuffer output;
        SkBufferDevice *bufferDevice = new SkBufferDevice;
        bufferDevice->open(output, SkBufferDeviceMode::BVM_ONLYWRITE);
        htmlWriter.setWriterDevice(bufferDevice, false);
        notify->writeHtml(&htmlWriter);
        htmlWriter.closeDocument();
        bufferDevice->close();
        bufferDevice->destroyLater();

        SkString code("$('#");
        code.append(t->command());
        code.append("_TRANS_RETMSG').html('");
        code.append(output.toString());
        code.append("');");

        //cout << "!!!!" << code <<  "\n";
        WebFrontendMountpoint::inject(code.c_str(), dynamic_cast<SkWebSocket *>(t->commander()));
        t->commander()->flush();
    }

    t->destroyLater();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebFrontendMountpoint::dbPairChanged(CStr *cmd, CStr *dbName, CStr *key, SkVariant &val)
{
    if (/*!fs || */frontends.isEmpty())
        return;

    if (SkString::compare(cmd, "SET"))
    {
        SkBinaryTreeVisit<SkString, WebPageWidgets *> *itr = pages.iterator();

        while(itr->next())
        {
            WebPageWidgets *page = itr->item().value();
            page->injectPairRunCodeToAll(dbName, key, val);
        }

        delete itr;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(WebFrontendMountpoint, onFastTick)
{
    SilentSlotArgsWarning();

    if (frontends.isEmpty())
        return;

    SkBinaryTreeVisit<SkString, WebPageWidgets *> *itr = pages.iterator();

    while(itr->next())
    {
        WebPageWidgets *page = itr->item().value();
        page->tick();
    }

    delete itr;
}

SlotImpl(WebFrontendMountpoint, onOneSecondTick)
{
    SilentSlotArgsWarning();

    if (pagesCheckChrono.stop() > 5.)
    {
        pagesCheckChrono.start();

        SkBinaryTreeVisit<SkString, WebPageWidgets *> *itr = pages.iterator();

        while(itr->next())
        {
            WebPageWidgets *page = itr->item().value();
            page->updateCheck();
        }

        delete itr;
    }

    if (templateManagerCheckChrono.stop() > 10.)
    {
        templateManagerCheckChrono.start();
        templateManager.updateCheck();

        SkBinaryTreeVisit<SkString, WebFrontendSocket *> *itr = frontends.iterator();

        while(itr->next())
        {
            WebFrontendSocket *frontend = itr->item().value();
            frontend->sck->sendPing("!", 1);
        }

        delete itr;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
