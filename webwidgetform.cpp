#include "webwidgetform.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WebWidgetForm, WebWidgetContainer)
{
    form = nullptr;

    t = WebWidgetType::WT_CONTAINER;
}

void WebWidgetForm::setPair(CStr *pairDbName, CStr *key)
{
    dbName = pairDbName;
    k = key;
}

void WebWidgetForm::build(SkArgsMap &formMap)
{
    SkString s;
    formMap.toString(s);
    //cout << s << "\n";

    SkString mapHash = stringHash(s);

    if (mapHash != lastFormMapHash)
    {
        lastFormMapHash = mapHash;
        lastFormMap = formMap;

        bool isControl = formMap["isControl"].toBool();

        if (form)
        {
            remove(form);
            form->destroyLater();
        }

        form = new WebWidgetContainer;

        {
            form->setID(k.c_str());

            SkArgsMap customize;

            customize["TITLE"] = formMap["lbl"];
            customize["DESC"] = formMap["description"];
            customize["OWNER_NAME"] = formMap["owner"];
            customize["OWNER_TYPE"] = formMap["ownerType"];
            customize["OWNER_SLOT"] = formMap["slot"];

            SkArgsMap m;
            m["defaultsForVariables"] = customize;

            if (isControl)
                form->setHtmlTemplate("form/control.html", m);

            else
                form->setHtmlTemplate("form/command.html", m);

            customize.clear();
            m.clear();
            customize["FORM_ID"] = k.c_str();
            m["defaultsForVariables"] = customize;

            form->setInitJsTemplate("jquery/form-set-submit.js", m);
        }

        add(form);

        SkVariantList params;
        formMap["params"].copyToList(params);

        SkAbstractListIterator<SkVariant> *paramsItr = params.iterator();

        while(paramsItr->next())
        {
            SkArgsMap fieldMap;
            paramsItr->item().copyToMap(fieldMap);

            SkString k = fieldMap["key"].toString();
            SkString lbl = fieldMap["lbl"].toString();
            SkVariant_T t = static_cast<SkVariant_T>(fieldMap["type"].toInt());
            SkString desc = fieldMap["desc"].toString();

            SkVariantList options;
            fieldMap["options"].copyToList(options);

            WebWidgetContainer *field = new WebWidgetContainer();
            field->setID(k.c_str());
            field->setLabel(lbl.c_str());

            SkVariant v;

            if (fieldMap.contains("value"))
                v = fieldMap["value"];

            else if (fieldMap.contains("dfltVal"))
                v = fieldMap["dfltVal"];

            SkArgsMap customize;
            customize["DESC"] = desc;

            SkString changingCode;

            if (!options.isEmpty())
            {
                SkArgsMap m;
                m["defaultsForVariables"] = customize;

                field->setHtmlTemplate("form/input-select.html", m);

                SkAbstractListIterator<SkVariant> *optionsItr = options.iterator();

                while(optionsItr->next())
                {
                    SkArgsMap optMap;
                    optionsItr->item().copyToMap(optMap);

                    SkString lbl = optMap["lbl"].toString();
                    SkString val = optMap["val"].toString();

                    WebWidgetContainer *option = new WebWidgetContainer();
                    option->setLabel(lbl.c_str());

                    SkArgsMap customize;
                    customize["VALUE"] = val;

                    if (v == val)
                        customize["SELECTED"] = "selected";

                    m["defaultsForVariables"] = customize;

                    option->setHtmlTemplate("form/input-option.html", m);

                    field->add(option);
                }

                delete optionsItr;
            }

            else if (t == T_BOOL)
            {
                if (v.toBool())
                    customize["CHECKED"] = "checked";

                SkArgsMap m;
                m["defaultsForVariables"] = customize;

                field->setHtmlTemplate("form/input-switch.html", m);
            }

            else if (SkVariant::isNumber(t))
            {
                float min = fieldMap["minVal"].toFloat();
                float max = fieldMap["maxVal"].toFloat();
                float step = fieldMap["stepVal"].toFloat();

                customize["VALUE"] = v;

                if (min != 0.)
                    customize["MIN"] = min;

                if (max > 0.)
                    customize["MAX"] = max;

                if (step > 0.)
                    customize["STEP"] = step;

                SkArgsMap m;
                m["defaultsForVariables"] = customize;

                if (max > 0.)
                {
                    field->setHtmlTemplate("form/input-slider.html", m);

                    changingCode.append("$('#");
                    changingCode.append(field->id());
                    changingCode.append("_OUTPUT').text(");
                    changingCode.append("$('#");
                    changingCode.append(field->id());
                    changingCode.append("').val());\n");

                    /*customize.clear();
                    m.clear();
                    customize["ID"] = field->id();
                    customize["EVENT"] = "input";

                    m["defaultsForVariables"] = customize;

                    if (isControl)
                    {
                        changingCode.append("$('#");
                        changingCode.append(form->id());
                        changingCode.append("').submit();\n");
                    }

                    SkArgsMap customizeBlock;
                    customizeBlock["ON_EVENT"] = changingCode;

                    m["defaultsForBlocks"] = customizeBlock;

                    field->setInitJsTemplate("jquery/element-set-event.js", m);*/
                }

                else
                    field->setHtmlTemplate("form/input-number.html", m);
            }

            else
            {
                customize["VALUE"] = v;

                SkArgsMap m;
                m["defaultsForVariables"] = customize;

                field->setHtmlTemplate("form/input-textline.html", m);
            }

            if (isControl)
            {
                changingCode.append("$('#");
                changingCode.append(form->id());
                changingCode.append("').submit();\n");
            }

            if (!changingCode.isEmpty())
            {
                customize.clear();
                customize["ID"] = field->id();
                customize["EVENT"] = "input";

                SkArgsMap customizeBlock;
                customizeBlock["ON_EVENT"] = changingCode;

                SkArgsMap m;
                m["defaultsForVariables"] = customize;
                m["defaultsForBlocks"] = customizeBlock;

                field->setInitJsTemplate("jquery/element-set-event.js", m);
            }

            form->add(field);
        }

        delete paramsItr;
    }
}

CStr *WebWidgetForm::pairsDatabaseName()
{
    return dbName.c_str();
}

CStr *WebWidgetForm::pairKey()
{
    return k.c_str();
}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
