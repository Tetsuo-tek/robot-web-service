#ifndef WEBFRONTENDMOUNTPOINT_H
#define WEBFRONTENDMOUNTPOINT_H

#include "webpagewidgets.h"

#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkHttpService;
class SkFlowSync;
class SkWorkerTransaction;

class WebFrontendMountpoint extends SkGenericMountPoint
{
    friend class WebPageBuilder;

    SkFlowAsync *async;

    SkString svrName;
    SkHttpService *service;

    SkTemplateManager templateManager;
    SkElapsedTime templateManagerCheckChrono;

    SkTreeMap<SkString, WebFrontendSocket *> frontends;//wsKey, frontendSck
    SkTreeMap<SkString, WebPageWidgets *> pages;
    SkElapsedTime pagesCheckChrono;

    public:
        Constructor(WebFrontendMountpoint, SkGenericMountPoint);

        bool init(CStr *serverName, SkFlowAsync *flow);
        void close();

        void dbPairChanged(CStr *cmd, CStr *dbName, CStr *key, SkVariant &val);

        void addWidgetContainer(WebWidgetContainer *wc);
        void addWidget(WebWidgetValue *w);

        static void inject(CStr *code, SkWebSocket *sck);

        void onWrkEvaluatedCmd(SkWorkerTransaction *t);

        Slot(appPageAccepted);

        Slot(onFrontendAccepted);
        Slot(onFrontendTextReadyRead);
        Slot(onFrontendDataReadyRead);
        Slot(onFrontendDisconnected);

        Slot(onWrkEvaluatedCmd);

        Slot(onFastTick);
        Slot(onOneSecondTick);

    private:
        bool initPages();
        bool buildRoutes(SkString &code);
        SkHtmlTemplate *buildMainPage(CStr *title, CStr *titleRef, CStr *footer);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // WEBFRONTENDMOUNTPOINT_H
