#include "webwidgetparser.h"
#include "webfrontendmountpoint.h"

#include <Core/System/Network/FlowNetwork/skflowasync.h>

WebWidgetParser::WebWidgetParser()
{
    sync = nullptr;
}

bool WebWidgetParser::parse(CStr *filePath, SkFlowAsync *flow)
{
    sync = flow->buildSyncClient();
    SkFsUtils::fillPathInfo(filePath, wuiPathInfo);

    // // //

    SkString output;

    if (!evaluateFile(filePath, output))
    {
        sync->close();
        sync->destroyLater();
        sync = nullptr;

        return false;
    }

    if (!widgets.fromString(output.c_str()))
    {
        sync->close();
        sync->destroyLater();
        sync = nullptr;

        return false;
    }

    // // //

    FlatMessage("Parsing widgets ..");
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = widgets.iterator();

    while(itr->next())
    {
        SkVariant &v = itr->item().value();

        if (itr->item().key() == "widgets" && !parseAbstractWidgetsList(v, nullptr))
        {
            delete itr;

            sync->close();
            sync->destroyLater();
            sync = nullptr;

            return false;
        }
    }

    delete itr;

    sync->close();
    sync->destroyLater();
    sync = nullptr;

    return true;
}

bool WebWidgetParser::evaluateFile(CStr *filePath, SkString &output)
{
    SkFileInfo *wuiFileInfo = new SkFileInfo;
    SkFsUtils::fillFileInfo(filePath, *wuiFileInfo, true);

    if (!wuiFileInfo->exists)
    {
        FlatError("Wui json-file NOT found: " << filePath);
        return false;
    }

    setup(filePath, nullptr);

    if (!build(output))
    {
        FlatError("Wui json-file NOT valid: " << wuiFileInfo->completeAbsolutePath);
        return false;
    }

    reset();
    onFileEvaluation(wuiFileInfo);
    return true;
}

#define CheckArgType(K, V, T) \
    FakeSingleLine( \
        hasError = (V.variantType() != T); \
        if (hasError) \
        { \
            FlatError("'" << K << "' MUST be: " << SkVariant::variantTypeName(T)); \
            continue; \
        })

bool WebWidgetParser::onKeyNotFound(CStr *k, SkString &replaced)
{
    SkStringList parsed;

    SkString s(k);
    s.split("_", parsed);

    SkString key;
    SkString dbName = "Main";

    if (parsed.count() == 1)
        key = k;

    else if (parsed.count() > 2)
    {
        dbName = parsed.first();

        if (parsed.count() == 2)
            key = parsed.last();

        else
        {
            parsed.removeFirst();
            key = parsed.join("_");
        }
    }

    else
    {
        FlatError("Key name is NOT valid: " << k);
        return false;
    }

    sync->setCurrentDbName(dbName.c_str());

    FlatWarning("CHECKING variable [dbName: " << dbName << "]: " << key);

    if (!sync->existsVariable(key.c_str()))
    {
        FlatWarning("Variable NOT found [dbName: " << dbName << "]: " << key);
        return false;
    }

    SkVariant v;
    sync->getVariable(key.c_str(), v);
    replaced = v.toString();

    if (replaced.isEmpty())
        FlatWarning("REPLACED variable IS EMPTY [dbName: " << dbName << "]: " << key);

    else
        FlatMessage("REPLACED variable [dbName: " << dbName << "]: " << key << " -> " << replaced);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// Module widget parsing

bool WebWidgetParser::parseAbstractWidgetsList(SkVariant &v, WebWidgetContainer *container)
{
    SkVariantList l;
    v.copyToList(l);

    SkAbstractListIterator<SkVariant> *itr = l.iterator();

    while(itr->next())
    {
        SkArgsMap m;
        itr->item().copyToMap(m);

        SkVariant &typeVal = m["type"];

        if (!typeVal.isString())
        {
            SkString json; \
            m.toString(json); \
            FlatError("'type' MUST be a T_STRING: " << json);
            delete itr;
            return false;
        }

        CStr *t = m["type"].data();

        if ((SkString::compare(t, "Container") && !parseWidgetContainer(m, container))
                || (SkString::compare(t, "Widget") && !parseWidget(m, container))
                || (SkString::compare(t, "Pairs") && !parseWidgetPairs(m, container))
                || (SkString::compare(t, "Form") && !parseWidgetForm(m, container)))
        {
            delete itr;
            return false;
        }
    }

    delete itr;
    return true;
}

bool WebWidgetParser::parseWidgetContainer(SkArgsMap &m, WebWidgetContainer *container)
{
    WebWidgetContainer *wc = new WebWidgetContainer;
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    bool hasError = false;

    while(itr->next() && !hasError)
    {
        CStr *k = itr->item().key().c_str();
        SkVariant &v = itr->item().value();

        if (SkString::compare(k, "id"))
        {
            CheckArgType(k, v, T_STRING);
            wc->setID(v.data());
        }

        else if (SkString::compare(k, "title"))
        {
            CheckArgType(k, v, T_STRING);
            wc->setLabel(v.data());
        }

        else if (SkString::compare(k, "html"))
        {
            CheckArgType(k, v, T_MAP);

            SkArgsMap m;
            v.copyToMap(m);

            SkString filePath = m["template"].toString();

            if (!filePath.isEmpty())
                wc->setHtmlTemplate(filePath.c_str(), m);
        }

        else if (SkString::compare(k, "init"))
        {
            CheckArgType(k, v, T_MAP);

            SkArgsMap m;
            v.copyToMap(m);

            SkString filePath = m["template"].toString();

            if (!filePath.isEmpty())
                wc->setInitJsTemplate(filePath.c_str(), m);
        }

        else if (SkString::compare(k, "tick"))
        {
            CheckArgType(k, v, T_MAP);

            SkArgsMap m;
            v.copyToMap(m);

            double iterval = m["interval"].toDouble();
            SkString filePath = m["template"].toString();

            if (!filePath.isEmpty())
                wc->setTickJsTemplate(filePath.c_str(), m, iterval);
        }

        else if (SkString::compare(k, "widgets"))
        {
            CheckArgType(k, v, T_LIST);

            if (!parseAbstractWidgetsList(v, wc))
            {
                delete itr;
                return false;
            }
        }

        else if (SkString::compare(k, "widgetsFilePath"))
        {
            CheckArgType(k, v, T_STRING);

            SkString wuiFilePath(SkFsUtils::adjustPathEndSeparator(wuiPathInfo.absoluteParentPath.c_str()));
            wuiFilePath.append(v.data());

            SkString wuiPartFilePath;

            if (!evaluateFile(wuiFilePath.c_str(), wuiPartFilePath))
                return false;

            if (!v.fromJson(wuiPartFilePath.c_str()))
            {
                hasError = true;
                FlatError("'widgetsFilePath' MUST contain a VALID json T_LIST");
                continue;
            }

            CheckArgType(k, v, T_LIST);

            hasError = !parseAbstractWidgetsList(v, wc);
        }
    }

    delete itr;

    if (hasError)
    {
        wc->destroyLater();
        return false;
    }

    if (container)
        container->add(wc);
    else
        addWidgetContainer(wc);

    return true;
}

bool WebWidgetParser::parseWidget(SkArgsMap &m, WebWidgetContainer *container)
{
    if (!container)
    {
        FlatError("A widget is ALWAYS a leaf and lives ALWAYS in a containers");
        return false;
    }

    WebWidgetValue *w = new WebWidgetValue;
    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    bool hasError = false;

    SkString pairDbName;
    SkString tempKeyWID;

    while(itr->next() && !hasError)
    {
        CStr *k = itr->item().key().c_str();
        SkVariant &v = itr->item().value();

        if (SkString::compare(k, "mode"))
        {
            CheckArgType(k, v, T_STRING);

            if (SkString::compare(v.data(), "Value"))
                w->setDataSourceType(WebWidgetSourceType::WST_VALUEWIDGET);

            else if (SkString::compare(v.data(), "Target"))
                w->setDataSourceType(WebWidgetSourceType::WST_TARGETWIDGET);

            else
            {
                hasError = true;
                FlatError("WidgetValue 'source' attribute UNKNOWN: " << v);
            }
        }

        else if (SkString::compare(k, "dbName"))
        {
            CheckArgType(k, v, T_STRING);

            pairDbName = v.data();
        }

        else if (SkString::compare(k, "key") || SkString::compare(k, "id"))
        {
            CheckArgType(k, v, T_STRING);
            tempKeyWID = v.data();
        }

        else if (SkString::compare(k, "label"))
        {
            CheckArgType(k, v, T_STRING);
            w->setLabel(v.data());
        }

        else if (SkString::compare(k, "min"))
            w->setMin(v);

        else if (SkString::compare(k, "max"))
            w->setMax(v);

        else if (SkString::compare(k, "udm"))
        {
            CheckArgType(k, v, T_STRING);
            w->setUdm(v.data());
        }

        else if (SkString::compare(k, "html"))
        {
            CheckArgType(k, v, T_MAP);

            SkArgsMap m;
            v.copyToMap(m);

            SkString filePath = m["template"].toString();

            if (!filePath.isEmpty())
                w->setHtmlTemplate(filePath.c_str(), m);
        }

        else if (SkString::compare(k, "init"))
        {
            CheckArgType(k, v, T_MAP);

            SkArgsMap m;
            v.copyToMap(m);

            SkString filePath = m["template"].toString();

            if (!filePath.isEmpty())
                w->setInitJsTemplate(filePath.c_str(), m);
        }

        else if (SkString::compare(k, "run"))
        {
            CheckArgType(k, v, T_MAP);

            SkArgsMap m;
            v.copyToMap(m);

            SkString filePath = m["template"].toString();

            if (!filePath.isEmpty())
                w->setRunJsTemplate(filePath.c_str(), m);
        }
    }

    delete itr;

    if (hasError)
    {
        w->destroyLater();
        return false;
    }

    if (w->widgetSourceType() == WST_VALUEWIDGET)
    {
        if (pairDbName.isEmpty())
        {
            FlatError("'dbName' CANNOT be EMPTY on a WidgetValue");
            w->destroyLater();
            return false;
        }

        if (tempKeyWID.isEmpty())
        {
            FlatError("'key' CANNOT be EMPTY on a WidgetValue");
            w->destroyLater();
            return false;
        }

        w->setPair(pairDbName.c_str(), tempKeyWID.c_str());
    }

    else if(w->widgetSourceType() == WST_TARGETWIDGET)
    {
        if (tempKeyWID.isEmpty())
        {
            FlatError("'key' CANNOT be EMPTY on a TargetValue");
            w->destroyLater();
            return false;
        }

        w->setID(tempKeyWID.c_str());
    }

    container->add(w);
    addWidget(w, sync);

    return true;
}

bool WebWidgetParser::parseWidgetPairs(SkArgsMap &m, WebWidgetContainer *container)
{
    SkString pairDbName;
    SkString pairKey;
    SkString udm;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    bool hasError = false;

    while(itr->next() && !hasError)
    {
        CStr *k = itr->item().key().c_str();
        SkVariant &v = itr->item().value();

        if (SkString::compare(k, "dbName"))
        {
            CheckArgType(k, v, T_STRING);
            pairDbName = v.toString();
        }

        else if (SkString::compare(k, "key"))
        {
            CheckArgType(k, v, T_STRING);
            pairKey = v.toString();
        }

        else if (SkString::compare(k, "udm"))
        {
            CheckArgType(k, v, T_STRING);
            udm = v.toString();
        }
    }

    delete itr;

    if (hasError)
        return false;

    if (pairDbName.isEmpty())
    {
        FlatError("'dbName' CANNOT be EMPTY on a WidgetValue");
        return false;
    }

    if (pairKey.isEmpty())
    {
        FlatError("'key' CANNOT be EMPTY on a WidgetValue");
        return false;
    }

    WebWidgetPairsTable *pairsContainer = new WebWidgetPairsTable;
    pairsContainer->setPair(pairDbName.c_str(), pairKey.c_str(), udm.c_str());

    return true;
}

bool WebWidgetParser::parseWidgetForm(SkArgsMap &m, WebWidgetContainer *container)
{
    SkString pairDbName;
    SkString pairKey;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = m.iterator();

    bool hasError = false;

    while(itr->next() && !hasError)
    {
        CStr *k = itr->item().key().c_str();
        SkVariant &v = itr->item().value();

        if (SkString::compare(k, "dbName"))
        {
            CheckArgType(k, v, T_STRING);
            pairDbName = v.toString();
        }

        else if (SkString::compare(k, "key"))
        {
            CheckArgType(k, v, T_STRING);
            pairKey = v.toString();
        }
    }

    delete itr;

    if (hasError)
        return false;

    if (pairDbName.isEmpty())
    {
        FlatError("'dbName' CANNOT be EMPTY on a WidgetValue");
        return false;
    }

    if (pairKey.isEmpty())
    {
        FlatError("'key' CANNOT be EMPTY on a WidgetValue");
        return false;
    }

    WebWidgetForm *formContainer = new WebWidgetForm;
    formContainer->setPair(pairDbName.c_str(), pairKey.c_str());

    SkArgsMap customize;

    customize["TAG"] = "div";
    customize["ATTRIBUTES"] = "class='row'";

    {
        SkArgsMap m;
        m["defaultsForVariables"] = customize;

        formContainer->setHtmlTemplate("container.html", m);
    }

    if (container)
        container->add(formContainer);
    else
        addWidgetContainer(formContainer);

    addWidgetForm(formContainer);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
