#include "webpagewidgets.h"
#include "webfrontendmountpoint.h"
#include "Core/System/Network/FlowNetwork/skflowserver.h"
#include <Core/System/Network/TCP/HTTP/skwebsocket.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

WebPageWidgets::WebPageWidgets()
{
    async = nullptr;
    templateManager = nullptr;
}

bool WebPageWidgets::init(CStr *dirPath, SkFlowAsync *flow, SkTemplateManager *templates)
{
    async = flow;

    SkFileInfo info;
    SkFsUtils::fillFileInfo(dirPath, info, true);

    if (info.suffix != "page")
    {
        FlatError("WidgetsPage directory name MUST have '.page' suffix; name is NOT valid: " << info.completeName);
        return false;
    }

    directory = dirPath;
    titleText = info.name;
    templateManager = templates;

    SkString name(objectName());
    name.append("[");
    name.append(info.name);
    name.append("]");
    setObjectName(name.c_str());

    SkString wuiFilePath = SkFsUtils::adjustPathEndSeparator(directory.c_str());
    wuiFilePath.append("wui.json");

    bool ok = parse(wuiFilePath.c_str(), async);

    if (ok)
        FlatMessage("Wui Parsed [" << wuiFileInfos.count() << " files]");

    return ok;
}

void WebPageWidgets::close()
{
    {
        SkAbstractListIterator<SkFileInfo *> *itr = wuiFileInfos.iterator();

        while(itr->next())
            delete itr->item();

        delete itr;
    }

    {
        SkAbstractListIterator<WebWidgetContainer *> *itr = containers.iterator();

        while(itr->next())
            itr->item()->destroyLater();

        delete itr;
    }

    wuiFileInfos.clear();
    containers.clear();
    pairSourceWidgets.clear();
    formWidgets.clear();
}

void WebPageWidgets::addWidgetContainer(WebWidgetContainer *wc)
{
    containers << wc;
}

void WebPageWidgets::addWidget(WebWidgetValue *w, SkFlowSync *sync)
{
    if (!w->container())
    {
        w->destroyLater();
        FlatError_EXT(w, "Widget has NOT a container: " << w->id());
    }

    else if (!w->hasHtmlTemplate() && !w->hasInitJsTemplate())
    {
        w->container()->remove(w);
        w->destroyLater();
        FlatError_EXT(w, "Widget has NOT html and NOT js: " << w->id());
    }

    else if (w->widgetSourceType() == WebWidgetSourceType::WST_NOTYPE)
    {
        w->container()->remove(w);
        w->destroyLater();
        FlatError_EXT(w, "Widget has NO TYPE: " << w->id());
    }

    else if (w->widgetSourceType() == WebWidgetSourceType::WST_VALUEWIDGET)
    {
        if (pairSourceWidgets.contains(w->id()))
        {
            FlatError_EXT(w, "Wui ALREADY exists, removing widget: " << w->id());
            w->container()->remove(w);
            w->destroyLater();
            return;
        }

        //if (!fs->existsOptionalPairDb(w->pairsDatabaseName()))
        if (!sync->existsOptionalPairDb(w->pairsDatabaseName()))
        {
            FlatWarning_EXT(w, "PairDatabase NOT found: " << w->pairsDatabaseName());
            w->container()->remove(w);
            w->destroyLater();
            return;
        }

        pairSourceWidgets[w->id()] = w;
        FlatMessage("ADDED widget as ValueWidget: [dbName: " << w->pairsDatabaseName() << ", key: " << w->pairKey() << ", wuiID: " << w->id() << "]");
    }

    else if (w->widgetSourceType() == WebWidgetSourceType::WST_TARGETWIDGET)
    {
        WebWidgetValue *source = nullptr;

        if (pairSourceWidgets.contains(w->id()))
            source = pairSourceWidgets[w->id()];

        else
        {
            FlatWarning("The selected 'widget' source is NOT found: " << w->id());
            w->container()->remove(w);
            w->destroyLater();
            return;
        }

        source->addTarget(w);
        FlatMessage("ADDED widget as TargetWidget from: [source: " << source->id() << ", wuiID: " << w->id() << "]");
    }
}

void WebPageWidgets::addWidgetForm(WebWidgetForm *f)
{
    formWidgets[f->pairKey()] = f;
    FlatMessage("ADDED form: [pairKey: " << f->pairKey() << "]");
}

void WebPageWidgets::onFileEvaluation(SkFileInfo *wuiFileInfo)
{
    FlatMessage("EVALUATED wui file: " << wuiFileInfo->completeAbsolutePath);
    wuiFileInfos << wuiFileInfo;
}

void WebPageWidgets::addFrontend(WebFrontendSocket *frontend, bool initialize)
{
    SkFlowSync *sync = async->buildSyncClient();
    frontend->currentPage = this;

    SkString code;

    if (initialize)
    {
        SkBinaryTreeVisit<SkString, WebWidgetForm *> *itr = formWidgets.iterator();

        while(itr->next())
        {
            WebWidgetForm *form = itr->item().value();

            //if (!db)
            if (!sync->existsOptionalPairDb(form->pairsDatabaseName()))
            {
                FlatError("Database NOT found: " << form->pairsDatabaseName());
                frontend->sck->disconnect();
                sync->close();
                sync->destroyLater();
                return;
            }

            sync->setCurrentDbName(form->pairsDatabaseName());

            if (!sync->existsVariable(form->pairKey()))
            {
                FlatError("Database pair NOT found: " << form->pairsDatabaseName() << "." << form->pairKey());
                frontend->sck->disconnect();
                sync->close();
                sync->destroyLater();
                return;
            }

            FlatMessage("Building dynamic form: " << form->pairsDatabaseName() << "." << form->pairKey());

            SkArgsMap formMap;
            SkVariant v;
            sync->getVariable(form->pairKey(), v);
            v.copyToMap(formMap);

            form->build(formMap);
        }

        delete itr;

        code = "document.querySelector(\"#mainSection\").innerHTML = `";

        SkString templateContent;

        if (!htmlCode(templateContent))
        {
            frontend->sck->disconnect();
            sync->close();
            sync->destroyLater();
            return;
        }

        code.append(templateContent);
        code.append("`;\n");

        if (!initCode(code))
        {
            frontend->sck->disconnect();
            sync->close();
            sync->destroyLater();
            return;
        }

        sync->setCurrentDbName("Main");

        SkStringList dbNames;
        SkVariant v;
        sync->getVariable("databases", v);
        v.copyToStringList(dbNames);

        for(uint64_t i=0; i<dbNames.count(); i++)
        {
            CStr *dbName = dbNames[i].c_str();

            sync->setCurrentDbName(dbName);
            buildAllDbPairsRunCode(dbName, code, sync);
        }

        WebFrontendMountpoint::inject(code.c_str(), frontend->sck);

        templateManager->clear();
    }

    frontendClients[frontend->sck->getKey()] = frontend;
    sync->close();
    sync->destroyLater();
}

void WebPageWidgets::delFrontend(WebFrontendSocket *frontend)
{
    frontend->currentPage = nullptr;
    frontendClients.remove(frontend->sck->getKey());
}

SkTreeMap<SkString, WebFrontendSocket *> &WebPageWidgets::frontends()
{
    return frontendClients;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebPageWidgets::inject(CStr *code)
{
    if (frontendClients.isEmpty())
        return;

    SkArgsMap m;
    m["command"] = "EVAL";
    m["code"] = code;

    SkString json;
    m.toString(json);

    SkBinaryTreeVisit<SkString, WebFrontendSocket *> *itr = frontendClients.iterator();

    while(itr->next())
    {
        WebFrontendSocket *frontendSck = itr->item().value();
        frontendSck->sck->write(json.c_str(), json.size());
    }

    delete itr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool WebPageWidgets::buildPairRunCode(CStr *dbName, CStr *k, SkVariant &v, SkString &code)
{
    if (SkString::isEmpty(dbName))
    {
        FlatError("DbName CANNOT be EMPTY");
        return false;
    }

    SkString val = v.toString();
    WebWidgetValue *w = pairSource(dbName, k);

    if (w)
    {
        //CODE WILL BE BUILT ALSO IF THIS W HAS NOT A runJs AND IT HA TARGET WITH runJs
        //RETURN false ONLY IF THIS W OR TARGET HAVE runJs BUT THERE ARE ERRORs
        if (!w->buildRunJs(templateManager, code, val))
        {
            templateManager->clear();
            FlatError("Cannot build RunJs template: " << w->runJsTemplate().filePath);
            return false;
        }
    }

    templateManager->clear();
    return true;
}

void WebPageWidgets::injectPairRunCodeToAll(CStr *dbName, CStr *k, SkVariant &v)
{
    if (frontendClients.isEmpty())
        return;

    SkString code;

    //sync->setCurrentDbName(dbName);

    if (buildPairRunCode(dbName, k, v, code) && !code.isEmpty())
        inject(code.c_str());
}

void WebPageWidgets::buildAllDbPairsRunCode(CStr *dbName, SkString &code, SkFlowSync *sync)
{
    SkArgsMap db;
    sync->getAllVariables(db);

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = db.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        buildPairRunCode(dbName, k.c_str(), v, code) && !code.isEmpty();
    }

    delete itr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *WebPageWidgets::title()
{
    return titleText.c_str();
}

WebWidgetValue *WebPageWidgets::pairSource(CStr *dbName, CStr *k)
{
    SkString keyExpanded;
    keyExpanded = dbName;
    keyExpanded.append("_");
    keyExpanded.append(k);

    if (pairSourceWidgets.contains(keyExpanded))
        return pairSourceWidgets[keyExpanded];

    return nullptr;
}

bool WebPageWidgets::htmlCode(SkString &html)
{
    SkAbstractListIterator<WebWidgetContainer *> *itr = containers.iterator();

    while(itr->next())
    {
        WebWidgetContainer *wc = itr->item();

        if (wc->hasHtmlTemplate() && !wc->buildHtml(templateManager)->build(html))
        {
            delete itr;
            return false;
        }
    }

    delete itr;
    templateManager->clear();
    return true;
}

bool WebPageWidgets::initCode(SkString &code)
{
    SkAbstractListIterator<WebWidgetContainer *> *itr = containers.iterator();

    while(itr->next())
    {
        AbstractWebWidget *wc = itr->item();

        //IT IS RECURSIVE
        //ALSO IF THE wc HAS NOT INIT CODE, ALL RECURSIVE CHILDREN COULD HAVE INIT CODE
        if (!wc->buildInitJs(templateManager, code))
        {
            templateManager->clear();
            delete itr;
            FlatError("Cannot build InitJs template for container: " << wc->id());
            return false;
        }
    }

    delete itr;
    templateManager->clear();
    return true;
}

bool WebPageWidgets::tickCode(SkString &code)
{
    SkAbstractListIterator<WebWidgetContainer *> *itr = containers.iterator();

    while(itr->next())
    {
        AbstractWebWidget *wc = itr->item();

        //IT IS RECURSIVE
        //ALSO IF THE wc HAS NOT TICK CODE, ALL RECURSIVE CHILDREN COULD HAVE TICK CODE
        if (!wc->buildTickJs(templateManager, code))
        {
            templateManager->clear();
            delete itr;
            FlatError("Cannot build TickJs template for container: " << wc->id());
            return false;
        }
    }

    delete itr;
    templateManager->clear();
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebPageWidgets::tick()
{
    if (frontendClients.isEmpty())
        return;

    SkString code;

    if (!tickCode(code))
        return;

    inject(code.c_str());
    templateManager->clear();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void WebPageWidgets::updateCheck()
{
    bool hasChanged = false;
    SkAbstractListIterator<SkFileInfo *> *itr = wuiFileInfos.iterator();

    while(itr->next() && !hasChanged)
    {
        SkFileInfo *wuiFileInfo = itr->item();
        SkFileInfo tempInfo;
        SkFsUtils::fillFileInfo(wuiFileInfo->completeAbsolutePath.c_str(), tempInfo, true);

        if (tempInfo.checkSum != wuiFileInfo->checkSum)
        {
            hasChanged = true;
            FlatWarning("A WuiFile has CHANGED: "
                      << wuiFileInfo->completeName
                      << " ["
                      << wuiFileInfo->checkSum
                      << " -> "
                      << tempInfo.checkSum
                      << "]");
        }
    }

    delete itr;

    if (hasChanged)
    {
        FlatMessage("Refactoring ..");
        close();
        init(wuiPathInfo.absoluteParentPath.c_str(), async, templateManager);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
